function [Z, U] = pcaReduce(X, k)
% pCAREDUCE Reduce the dimensions of input X using PCA
%	[Z, U] = PCAREDUCE(X, k) uses PCA algorithm for dimensionality reduction on
%	input X. k is number of principal components to be used for reduction.

% Normalize the input (required for PCA)
X_norm = normalize(X);

% Apply PCA
[U S V] = svd(1./size(X_norm,1) * X_norm'*X);

% Select optimal number of principal components
if ~exist('k', 'var') || isempty(k)
	SDiag = diag(S);
	Ssum = sum(SDiag);
	for k=1:size(X_norm, 2)
		if sum(SDiag(1:k))/Ssum >= 0.9
			break
		end
	end
end

fprintf('pcaReduce: k=%d', k);

% project the data
U = U(:,1:k);
Z = X_norm*U;

end;


