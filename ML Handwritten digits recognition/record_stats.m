function record_stats(iter, num_hidden_layers, hidden_layer_size, cost, accuracy, stats)
% RECORD_STATS Records the stats of the current run into file "stats"
%	RECORD_STATS(iter, num_hidden_layers, hidden_layer_size, cost, accuracy, stats)
%	saves the input parameters in file "stats".

if ~exist('iter', 'var') || isempty(iter) || ~exist('num_hidden_layers', 'var') || isempty(num_hidden_layers) ||~exist('hidden_layer_size', 'var') || isempty(hidden_layer_size) ||~exist('cost', 'var') || isempty(cost) ||~exist('accuracy', 'var') || isempty(accuracy)
	fprintf(stderr, 'record_stats: Error: Arguments not supplied\n');
	return
end

% set to default if not passed in
if ~exist('stats', 'var') || isempty(stats) 
	stats = 'stats';
end


fid = fopen(stats, "a");
if fid == -1
	fprintf(stderr, 'record_stats: Error: Unable to open file "stats" for writing.\n')
	return
end

fprintf(fid, '\t%d\t|\t%d\t|\t%s\t|\t%f\t|\t%f\n', iter, num_hidden_layers, sprintf('%d ', hidden_layer_size), cost(end), accuracy);
fclose(fid);

end
