#!/bin/bash
# generate_graph.sh
# Generate 'Accuracy vs hidden units' graphs using data from files 'stats' & 'results'.

# set "iters" to default if not supplied
iters=${1:-150}
touch acc hu racc rhu
sort -t'|' -n -k 3 stats   | awk -F'|' '$1 == '$iters' { printf("%s\n",$3)>>"hu";  printf("%s\n", $5)>>"acc"  }'
sort -t'|' -n -k 3 results | awk -F'|' '$1 == '$iters' { printf("%s\n",$3)>>"rhu"; printf("%s\n", $5)>>"racc" }'

octave -q --persist --eval "acc = load('acc'); hu = load('hu');
	racc = load('racc'); rhu = load('rhu');
	plot(hu, acc, '--ok', 'markerfacecolor', 'r');
	hold on;
	plot(rhu, racc, '--ok', 'markerfacecolor', 'b');
	legend('Training set', 'Test set');
	xlabel('# of hidden units');
	ylabel('Accuracy in %');
	title('Accuracy vs # of hidden units (iters=$iters)');
	printf('Graph Generated.\nPress ^D to exit\n');"

rm acc hu racc rhu
