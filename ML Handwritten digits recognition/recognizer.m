% TODO: Add final results graph

load "xstest.mat"
load "Theta1.mat"
load "Theta2.mat"
load "Ureduce.mat"

Z = normalize(xstest)*Ureduce;
pred = predict(Theta1, Theta2, Z);

displayData(xstest);
printf("Results:\n");
pred(pred == 10) = 0;
printf("%d\n", pred);
