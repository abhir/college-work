%% Initialization
clear ; close all; clc

num_hidden_layers = 1;
hidden_layer_size = 300;   % keep at least 100
iter = 150;		% keep at least 100
num_labels = 10;          % 10 labels, from 1 to 10; 10 represents 0

% =======================================

fprintf('\nLoading data... \n');

X = load_images('train-images-idx3-ubyte');
y = load_labels('train-labels-idx1-ubyte');

% Reduce using PCA
k = 100;	% number of principal components
[X, Ureduce] = pcaReduce(X, k);

% Initialize sizes
m = size(X, 1);
input_layer_size  = size(X, 2);

% =======================================

%{
% Randomly select 100 data points to display
sel = randperm(size(X, 1));
sel = sel(1:100);

displayData(X(sel, :));

fprintf('Program paused. Press enter to continue.\n');
pause;
%}

% =======================================

% Initialize Neural Network Parameters 

initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);

% Unroll parameters
initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

% =======================================

fprintf('\nTraining Neural Network... \n')

options = optimset('MaxIter', iter);

%  You should also try different values of lambda
lambda = 0;

% Create "short hand" for the cost function to be minimized
costFunction = @(p) nnCostFunction(p, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, X, y, lambda);

% Now, costFunction is a function that takes in only one argument (the
% neural network parameters)
[nn_params, cost] = fmincg(costFunction, initial_nn_params, options);

% Obtain Theta1 and Theta2 back from nn_params
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% =======================================

% Plot the convergence graph
figure;
plot(1:numel(cost), cost, '-b', 'LineWidth', 2);
hold on;
xlabel('Number of iterations');
ylabel('Cost J'); 

% =======================================

%{
fprintf('\nVisualizing Neural Network... \n')

% Create new figure for visualization
figure;
displayData(Theta1(:, 2:end));

fprintf('\nProgram paused. Press enter to continue.\n');
pause;
%}

% =======================================

% predict labels using calculated parameters
pred = predict(Theta1, Theta2, X);
accuracy = mean(double(pred == y)) * 100;
fprintf('\nTraining Set Accuracy: %f\n', accuracy);

% =======================================

% record the stats of training run
record_stats(iter, num_hidden_layers, hidden_layer_size, cost, accuracy, 'stats');

% =======================================

%{
fprintf('\nRunning examples...\n');

%  Randomly permute examples
rp = randperm(m);

for i = 1:m
    % Display 
    fprintf('\nDisplaying Example Image\n');
    displayData(X(rp(i), :));

    pred = predict(Theta1, Theta2, X(rp(i),:));
    fprintf('\nNeural Network Prediction: %d (digit %d)\n', pred, mod(pred, 10));
    
    % Pause
    fprintf('Program paused. Press enter to continue.\n');
    pause;
end

%}

% =======================================

% Perform forward propagation on Test Set and compute results

fprintf('\nLoading test data...\n');

Xtest = load_images('t10k-images-idx3-ubyte');
ytest = load_labels('t10k-labels-idx1-ubyte');

% normalize and map to eigenvectors 
Xtest = normalize(Xtest)*Ureduce;

cost_test = nnCostFunction(nn_params, input_layer_size, hidden_layer_size, num_labels, Xtest, ytest, 0);
fprintf('\nCost (J) for Test Set: %f\n', cost_test);

pred_test = predict(Theta1, Theta2, Xtest);
accuracy_test = mean(double(pred_test == ytest)) * 100;
fprintf('\nTest Set Accuracy: %f\n', accuracy_test);

% =======================================

% record the stats of test run
record_stats(iter, num_hidden_layers, hidden_layer_size, cost_test, accuracy_test, 'results');

% =======================================
